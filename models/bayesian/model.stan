data {
    int<lower=1> n_time_periods;  // number of update steps of random walk
    int<lower=1> n_compartments;  // number of compartments

    // Variables around bees and colonies in low density compartments
    int<lower=1> n_low_observations; // number of bees observed
    int<lower=1> n_low_colonies; // number of colonies
    int<lower=1,upper=5> low_intensity[n_low_observations]; // observed intensity of infection in each observed bee
    int low_observation_colony_lookup[n_low_observations]; // the id of the colony the bee of each observation belongs to
    int low_observation_time_period_lookup[n_low_observations]; // which time period each observation came from
    int low_colony_compartment_lookup[n_low_colonies]; // which compartment each colony is in
    int low_density; // number of colonies in a low density compartment
    int low_compartment_map[n_low_colonies, low_density-1]; // the ith row is a list of colonies in the same compartment as colony i
    int is_low_colony_inoculated[n_low_colonies]; // 1 if colony was inoculated, 0 otherwise

    // Variables around bees and colonies in high density compartments
    int<lower=1> n_high_observations; // number of bees observed
    int<lower=1> n_high_colonies; // number of colonies
    int<lower=1,upper=5> high_intensity[n_high_observations]; // observed intensity of infection in each observed bee
    int high_observation_colony_lookup[n_high_observations]; // the id of the colony the bee of each observation belongs to
    int high_observation_time_period_lookup[n_high_observations]; // which time period each observation came from
    int high_colony_compartment_lookup[n_high_colonies]; // which compartment each colony is in
    int high_density; // number of colonies in a low density compartment
    int high_compartment_map[n_high_colonies, high_density-1]; // the ith row is a list of colonies in the same compartment as colony i
    int is_high_colony_inoculated[n_high_colonies]; // 2 if colony was inoculated, 1 otherwise (Stan indexes from 1)

    real cutoff_prior; // Variance parameter of cauchy prior of cutoffs
    real sigma_alpha; // Variance parameter of prior for low density transmission rate
    real sigma_beta; // Variance parameter of prior on incremental transmission rate in high density compartments
    real sigma_prior; // Parameter of prior on variance of random walk step
}
parameters {
    matrix[n_time_periods, n_low_colonies] low_latent_intensity; // latent intensity of each colony in low density compartment through time
    matrix[n_time_periods, n_high_colonies] high_latent_intensity; // latent intensity of each colony in high density compartment through time

    // tuple of transmission rate in low density compartment
    // first element represents transmission from non-inoculated to inoculated
    // second element represents transmission from inoculated to non-inoculated
    vector[2] alpha;

    // tuple of incremental transmission rate in high density compartment
    // transmission rate in high density compartments is alpha + beta
    // first element represents transmission from non-inoculated to inoculated
    // second element represents transmission from inoculated to non-inoculated
    vector[2] beta;

    ordered[4] cutoffs; // threshold values of latent intensity needed to reach observed intensities
    real<lower=0> sigma; // variance of random walk update step
}

model {

    // uninformative prior that allows for extremely uneven step sizes
    cutoffs ~  cauchy(0, cutoff_prior);

    // uninformative, strictly positive prior as we have little knowledge of reasonable values
    sigma ~ gamma(sigma_prior, sigma_prior);

    // uninformative normal distributions used for computational efficiency
    alpha ~ normal(0, sigma_alpha);
    beta ~ normal(0, sigma_beta);

    // set priors on starting intensity of infection for colonies in low density compartments
    // inoculated colonies allowed to start much higher as we know they're infected
    for (j in 1:n_low_colonies){
        if(is_low_colony_inoculated[j]==2){
           low_latent_intensity[1,j] ~ normal(0, 5);
        }
        if(is_low_colony_inoculated[j]==1){
           low_latent_intensity[1,j] ~ normal(0, 0.5);
        }
    }

    // update belief about latent level of infection of colonies in low density compartments through time
    for (i in 2:n_time_periods){
        for (j in 1:n_low_colonies){
            real current_period_intensity;
            // start with the latent intensity in the previous period
            current_period_intensity = low_latent_intensity[i-1,j];

            // look at the other colonies in the same compartment
            for (k in 1:(low_density-1)){
                int is_k_inoculated_colony;

                // check whether the other colony is the inoculated one
                is_k_inoculated_colony = is_low_colony_inoculated[low_compartment_map[j,k]];

                // non-inoculated colonies get boosted by the inoculated colony
                // amount of boosting depends on transmission rate and the level of infection in the other colony
                if (is_low_colony_inoculated[j] == 1){
                     if (is_k_inoculated_colony == 2) {
                         current_period_intensity = current_period_intensity + (alpha[2] * low_latent_intensity[i-1, low_compartment_map[j,k]]);
                    }
                }
                // inoculated colonies get boosted by the non-inoculated colonies
                // amount of boosting depends on transmission rate and the level of infection in the other colony
                if (is_low_colony_inoculated[j] == 2){
                    current_period_intensity = current_period_intensity + ((alpha[1] / (low_density - 1)) * low_latent_intensity[i-1, low_compartment_map[j,k]]);
                }
            }
            // update belief about latent intensity in the next period
            low_latent_intensity[i,j] ~ normal(current_period_intensity, sigma);
        }
    }

    // set priors on starting intensity of infection for colonies in high density compartments
    // inoculated colonies allowed to start much higher as we know they're infected
    for (j in 1:n_high_colonies){
        if(is_high_colony_inoculated[j]==2){
           high_latent_intensity[1,j] ~ normal(0, 5);
        }
        if(is_high_colony_inoculated[j]==1){
           high_latent_intensity[1,j] ~ normal(0, 0.5);
        }
    }

    // update belief about infectedness of colonies in high density compartments through time
    for (i in 2:n_time_periods){
        for (j in 1:n_high_colonies){
            real current_period_intensity;
            // start with the latent intensity in the previous period
            current_period_intensity = high_latent_intensity[i-1,j];

            // look at the other colonies in the same compartment
            for (k in 1:(high_density-1)){
                int is_k_inoculated_colony;

                // check whether the other colony is the inoculated one
                is_k_inoculated_colony = is_high_colony_inoculated[high_compartment_map[j,k]];

                // non-inoculated colonies get boosted by the inoculated colony
                // amount of boosting depends on transmission rate and the level of infection in the other colony
                if (is_high_colony_inoculated[j] == 1){
                     if (is_k_inoculated_colony == 2) {
                         current_period_intensity = current_period_intensity + ((alpha[2] + beta[2]) * high_latent_intensity[i-1, high_compartment_map[j,k]]);
                    }
                }
                // inoculated colonies get boosted by the non-inoculated colonies
                // amount of boosting depends on transmission rate and the level of infection in the other colony
                if (is_high_colony_inoculated[j] == 2){
                    current_period_intensity = current_period_intensity + (((alpha[1] + beta[1]) / (high_density - 1)) * high_latent_intensity[i-1, high_compartment_map[j,k]]);
                }
            }
            // update belief about latent intensity in the next period
            high_latent_intensity[i,j] ~ normal(current_period_intensity, sigma);
        }
    }

    // control of the observations from low density compartments
    for (o in 1:n_low_observations){
        low_intensity[o] ~ ordered_logistic(low_latent_intensity[low_observation_time_period_lookup[o], low_observation_colony_lookup[o]], cutoffs);
    }

    // control of the observations from high density compartments
    for (o in 1:n_high_observations){
        high_intensity[o] ~ ordered_logistic(high_latent_intensity[high_observation_time_period_lookup[o], high_observation_colony_lookup[o]], cutoffs);
    }

}

generated quantities {

    // replication of intensity of infection in each observation from low density compartments
    int<lower=0> low_intensity_star[n_low_observations];
    // replication of intensity of infection in each observation from high density compartments
    int<lower=0> high_intensity_star[n_high_observations];

    int proposal;
    // for each observation in a low density compartment, draw a replicate form the posterior
    // 1 subtracted from the replicate to convert from Stan's 1 indexing of lists to python's 0 indexing
    for (o in 1:n_low_observations){
        proposal = ordered_logistic_rng(low_latent_intensity[low_observation_time_period_lookup[o], low_observation_colony_lookup[o]], cutoffs);
        low_intensity_star[o] = proposal - 1;
    }

    // for each observation in a high density compartment, draw a replicate form the posterior
    // 1 subtracted from the replicate to convert from Stan's 1 indexing of lists to python's 0 indexing
    for (o in 1:n_high_observations){
        proposal = ordered_logistic_rng(high_latent_intensity[high_observation_time_period_lookup[o], high_observation_colony_lookup[o]], cutoffs);
        high_intensity_star[o] = proposal - 1;
    }

}