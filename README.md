# Bees Density And Parasite Transmission

This repository contains the code for the statisitcal analyses carried out in Bailes et al. "Host density drives viral, but not trypanosome, transmission in a key pollinator" (submitted)

The code for analyses can be found in the models folder:
Linear mixed models, survival analyses, binomal models and ordinal models are all detailed in the R markdown file located in the "other_models" folder
Bayseian analyses are found in the "bayesain" folder

The data files that these analyses run from are currently found in the electronic supplementary files associated with the manuscript (Bailesetal_datafile1-7_ESM). A description of each of these files is also included within the elctronic supplementary files